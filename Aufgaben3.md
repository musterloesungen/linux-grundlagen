## Übungen zum Prozessmanagement in Linux

Die folgenden Übungen sollen das Verständnis und Fähigkeiten im Umgang mit Linux-Prozessmanagement-Tools wie `ps`, `top`, `pstree`, `pgrep`, `pkill`, und `kill` vertiefen. 

### Übung 1: Prozesse auflisten und analysieren

1. **Grundlegende Prozessinformationen anzeigen**: Verwenden Sie `ps aux` und identifizieren Sie die Bedeutung jeder Spalte in der Ausgabe.
   
2. **Dynamische Prozessansicht**: Nutzen Sie `top` oder `htop` (falls installiert), um die aktuell laufenden Prozesse dynamisch zu überwachen. Konzentrieren Sie sich dabei auf die CPU- und Speichernutzung.

3. **Baumstruktur der Prozesse**: Verwenden Sie `pstree`, um die Hierarchie der Prozesse anzuzeigen. Untersuchen Sie, wie Eltern- und Kindprozesse organisiert sind.

### Übung 2: Spezifische Prozesse finden und filtern

1. **Prozesse nach Namen finden**: Verwenden Sie `pgrep` gefolgt vom Namen eines laufenden Prozesses (z.B. `bash`), um dessen PID(s) zu finden.

2. **Prozesse detailliert filtern**: Kombinieren Sie `ps` mit `grep`, um Prozesse zu finden, die einen bestimmten String im Namen haben. Beispiel: `ps aux | grep ssh`.

3. **Prozesse nach Benutzer filtern**: Listen Sie alle Prozesse auf, die von einem bestimmten Benutzer (z.B. `root`) ausgeführt werden, unter Verwendung von `ps` und dem entsprechenden Filter.

### Übung 3: Prozesse beenden und Priorität ändern

1. **Einen Prozess sicher beenden**: Finden Sie einen Prozess mit `pgrep` oder `ps` und beenden Sie ihn mit `kill` gefolgt von der PID. Verwenden Sie zuerst `SIGTERM` (Signal 15) und, falls notwendig, `SIGKILL` (Signal 9).

2. **Prozesspriorität ändern**: Verwenden Sie `nice` beim Starten eines neuen Prozesses, um ihm eine niedrigere Priorität zu geben. Ändern Sie dann die Priorität eines laufenden Prozesses mit `renice`.

3. **Prozesse neu starten**: Beenden Sie einen Prozess und starten Sie ihn sofort neu, ohne das Terminal zu schließen oder den Befehl erneut einzugeben (Tipp: Nutzen Sie `exec` in Kombination mit Prozessmanagement-Befehlen).

### Übung 4: Erweiterte Prozessüberwachung

1. **Spezifische Prozessdetails anzeigen**: Verwenden Sie `ps -o pid,ppid,cputime,cmd -p [PID]`, um spezifische Details eines Prozesses anzuzeigen.

2. **Überwachung der Prozessaktivität**: Nutzen Sie `watch` zusammen mit `ps` für einen bestimmten Prozess, um dessen Aktivität in Echtzeit zu überwachen.

3. **Benachrichtigung bei Prozessende**: Schreiben Sie ein einfaches Shell-Skript, das eine Benachrichtigung ausgibt, wenn ein bestimmter Prozess nicht mehr läuft.

### Übung 5: Systemweite Prozessanalyse

1. **Systemweite Ressourcennutzung analysieren**: Nutzen Sie `top` oder `htop`, um die Prozesse zu identifizieren, die die meisten Ressourcen verbrauchen. Sortieren Sie die Ausgabe nach CPU- und Speichernutzung.

2. **Finden von Zombie-Prozessen**: Identifizieren Sie Zombie-Prozesse auf Ihrem System und überlegen Sie Strategien, wie diese bereinigt werden können.

3. **Lang laufende Prozesse identifizieren**: Verwenden Sie `ps` in Kombination mit `sort` und `awk` oder `cut`, um die am längsten laufenden Prozesse auf Ihrem System zu ermitteln.

Diese Übungen decken eine breite Palette von Fähigkeiten im Prozessmanagement ab und sollten Ihnen helfen, ein tieferes Verständnis für das Monitoring und die Steuerung von Prozessen unter Linux zu entwickeln.
