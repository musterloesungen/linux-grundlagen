## Übungsaufgaben zu Linux-Dateibefehlen

### Aufgabe 1: Auflisten von Dateien und Verzeichnissen
Verwenden Sie den Befehl `ls` mit verschiedenen Optionen, um:

1. Alle Dateien und Verzeichnisse im aktuellen Verzeichnis aufzulisten.
2. Alle Dateien und Verzeichnisse, einschließlich versteckter Dateien, aufzulisten.
3. Die Dateien und Verzeichnisse detailliert aufzulisten, inklusive Dateiberechtigungen, Anzahl der Links, Eigentümer, Gruppe, Größe und Zeitstempel.

### Aufgabe 2: Erstellen und Löschen von Dateien
1. Erstellen Sie eine neue leere Datei namens `testdatei.txt` im aktuellen Verzeichnis.
2. Löschen Sie die Datei `testdatei.txt` sicher, ohne eine Bestätigungsaufforderung.

### Aufgabe 3: Erstellen und Navigieren von Verzeichnissen
1. Erstellen Sie ein neues Verzeichnis namens `neues_verzeichnis`.
2. Wechseln Sie in das Verzeichnis `neues_verzeichnis`.
3. Kehren Sie zum übergeordneten Verzeichnis zurück.

### Aufgabe 4: Kopieren und Verschieben von Dateien
1. Erstellen Sie eine Kopie der Datei `testdatei.txt` mit dem Namen `kopie_testdatei.txt` im aktuellen Verzeichnis.
2. Verschieben Sie die Datei `kopie_testdatei.txt` in das Verzeichnis `neues_verzeichnis`.

### Aufgabe 5: Bearbeiten von Dateiberechtigungen
1. Ändern Sie die Berechtigungen der Datei `testdatei.txt`, so dass nur der Eigentümer die Datei lesen und schreiben kann.
2. Ändern Sie die Berechtigungen des Verzeichnisses `neues_verzeichnis`, so dass niemand außer dem Eigentümer den Inhalt lesen, schreiben oder ausführen kann.

### Aufgabe 6: Durchsuchen von Dateiinhalten
1. Suchen Sie in der Datei `testdatei.txt` nach dem Wort "Linux".
2. Zeigen Sie die ersten 10 Zeilen der Datei `testdatei.txt` an.
3. Zeigen Sie die letzten 10 Zeilen der Datei `testdatei.txt` an.

### Aufgabe 7: Verwenden von Pipes und Redirection
1. Leiten Sie die Ausgabe des Befehls `ls -l` in eine Datei namens `verzeichnisinhalt.txt` um.
2. Benutzen Sie `grep`, um alle Zeilen in `verzeichnisinhalt.txt` zu finden, die `testdatei.txt` enthalten, und leiten Sie das Ergebnis in eine neue Datei um.

### Aufgabe 8: Anzeigen von Dateitypen
1. Verwenden Sie den Befehl `file`, um den Typ der Datei `testdatei.txt` anzuzeigen.

### Aufgabe 9: Nutzung von Wildcards
1. Erstellen Sie mehrere Dateien gleichzeitig, die mit `datei` beginnen und unterschiedliche Endungen haben, z.B. `.txt`, `.pdf`, `.doc`.
2. Löschen Sie alle Dateien, die mit `datei` beginnen und die Endung `.txt` haben.

### Aufgabe 10: Finden von Dateien und Verzeichnissen
1. Verwenden Sie `find`, um alle Dateien im aktuellen Verzeichnis zu suchen, die größer als 10MB sind.
2. Verwenden Sie `find`, um alle Verzeichnisse zu suchen, die den Namen `neues_verzeichnis` tragen.