# Übungsaufgaben für Linux-Schulung

## Aufgabe 1: Dateiverwaltung

1. Erstellen Sie ein Verzeichnis namens `linux_schulung` in Ihrem Home-Verzeichnis.
2. Wechseln Sie in das Verzeichnis `linux_schulung`.
3. Erstellen Sie eine leere Datei namens `test.txt`.
4. Benutzen Sie `echo`, um den Text "Hallo Linux Welt" in `test.txt` zu schreiben.
5. Verwenden Sie `ls -l`, um die Details von `test.txt` anzuzeigen.
6. Erstellen Sie einen symbolischen Link für `test.txt` mit dem Namen `link_test.txt`.
7. Erstellen Sie ein Unterverzeichnis namens `backup`.
8. Verschieben Sie `test.txt` in das Verzeichnis `backup`.
9. Löschen Sie den symbolischen Link `link_test.txt`.

## Aufgabe 2: Erweiterte Dateiverwaltung

1. Erstellen Sie mehrere Dateien gleichzeitig mit `touch file1.txt file2.txt file3.txt`.
2. Verwenden Sie `ls -a`, um alle Dateien (einschließlich versteckter Dateien) im aktuellen Verzeichnis anzuzeigen.
3. Erstellen Sie einen harten Link für `file1.txt` mit dem Namen `hardlink_file1.txt`.
4. Verwenden Sie `ls -i`, um die Inode-Nummern von `file1.txt` und `hardlink_file1.txt` zu vergleichen.
5. Löschen Sie `file2.txt` und überprüfen Sie mit `ls`, dass die Datei gelöscht wurde.
6. Verwenden Sie `mkdir`, um zwei Verzeichnisse `dir1` und `dir2` zu erstellen.
7. Verschieben Sie `file3.txt` nach `dir1` und dann von `dir1` nach `dir2`.

## Aufgabe 3: Nutzung von `man` und `ln`

1. Verwenden Sie `man touch`, um die Dokumentation für den `touch`-Befehl zu lesen und herauszufinden, wie man das Änderungsdatum einer Datei aktualisiert.
2. Erstellen Sie eine Datei namens `man_ls.txt` und leiten Sie die Ausgabe von `man ls` in diese Datei um.
3. Verwenden Sie `ln -s` um einen symbolischen Link für das Verzeichnis `dir2` zu erstellen, benennen Sie diesen Link `sym_dir2`.
4. Überprüfen Sie mit `ls -l`, dass `sym_dir2` korrekt auf `dir2` zeigt.

## Aufgabe 4: Datei- und Verzeichnisrechte

1. Verwenden Sie `chmod`, um die Berechtigungen von `file1.txt` so zu ändern, dass nur der Besitzer lesen und schreiben kann.
2. Erstellen Sie eine Datei `secret.txt` und ändern Sie die Berechtigungen, sodass niemand außer dem Besitzer sie lesen kann.
3. Verwenden Sie `ls -l`, um die Berechtigungen für `secret.txt` zu überprüfen.
4. Löschen Sie das Verzeichnis `backup` und alle darin enthaltenen Dateien.

Diese Aufgaben decken grundlegende und etwas fortgeschrittene Konzepte der Linux-Dateiverwaltung ab und sollten den Teilnehmern helfen, ihr Verständnis und ihre Fähigkeiten in der Nutzung des Linux-Befehlszeileninterface zu vertiefen.
