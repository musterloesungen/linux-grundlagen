# Shell-Scripting (2)

## Aufgabe 8: Mehrere Bedingungen prüfen

### Aufgabe
Erstelle ein Script, das überprüft, ob ein Benutzer sowohl eine Datei als auch ein Verzeichnis angibt. Falls beides vorhanden ist, gib aus: **"Datei und Verzeichnis existieren."** Falls nicht, gib an, was fehlt.

### Anleitung
1. Nutze `[ -f ]` für Dateien und `[ -d ]` für Verzeichnisse.
2. Nutze `&&` und `||`, um mehrere Bedingungen zu prüfen.
3. Beispiel:
   ```bash
   ./script.sh datei.txt verzeichnis/
   ```

### Musterlösung
```bash
#!/bin/bash

DATEI=$1
VERZEICHNIS=$2

if [ -f "$DATEI" ] && [ -d "$VERZEICHNIS" ]; then
    echo "Datei und Verzeichnis existieren."
else
    if [ ! -f "$DATEI" ]; then
        echo "Die Datei $DATEI existiert nicht."
    fi
    if [ ! -d "$VERZEICHNIS" ]; then
        echo "Das Verzeichnis $VERZEICHNIS existiert nicht."
    fi
fi
```

---

## Aufgabe 9: Benutzereingaben prüfen

### Aufgabe
Erstelle ein Script, das den Benutzer auffordert, eine Zahl zwischen 1 und 10 einzugeben. Überprüfe, ob die Eingabe korrekt ist. Falls nicht, gib eine Fehlermeldung aus.

### Anleitung
1. Nutze `read`, um die Eingabe des Benutzers zu erfassen.
2. Prüfe die Eingabe mit `if` und `[ ]`, z. B. `[ "$ZAHL" -ge 1 ] && [ "$ZAHL" -le 10 ]`.
3. Beispiel:
   ```bash
   ./script.sh
   ```

### Musterlösung
```bash
#!/bin/bash

echo "Bitte gib eine Zahl zwischen 1 und 10 ein:"
read ZAHL

if [ "$ZAHL" -ge 1 ] && [ "$ZAHL" -le 10 ]; then
    echo "Danke, deine Zahl ist $ZAHL."
else
    echo "Ungültige Eingabe. Bitte eine Zahl zwischen 1 und 10 eingeben."
fi
```

---

## Aufgabe 10: Schleifen und Arrays

### Aufgabe
Erstelle ein Script, das ein Array mit fünf Namen enthält und jeden Namen einzeln ausgibt.

### Anleitung
1. Definiere ein Array mit `NAMEN=("Anna" "Ben" "Clara" "David" "Eva")`.
2. Nutze eine `for`-Schleife, um durch das Array zu iterieren.
3. Beispielausgabe:
   ```
   Anna
   Ben
   Clara
   David
   Eva
   ```

### Musterlösung
```bash
#!/bin/bash

NAMEN=("Anna" "Ben" "Clara" "David" "Eva")

for NAME in "${NAMEN[@]}"; do
    echo "$NAME"
done
```

---

## Aufgabe 11: Dateien zählen

### Aufgabe
Erstelle ein Script, das die Anzahl der Dateien in einem Verzeichnis zählt und ausgibt.

### Anleitung
1. Nutze `ls` oder `find`, um die Dateien im Verzeichnis aufzulisten.
2. Zähle die Dateien mit `wc -l`.
3. Beispiel:
   ```bash
   ./script.sh verzeichnis/
   ```

### Musterlösung
```bash
#!/bin/bash

VERZEICHNIS=$1

if [ -d "$VERZEICHNIS" ]; then
    ANZAHL=$(ls -1 "$VERZEICHNIS" | wc -l)
    echo "Das Verzeichnis $VERZEICHNIS enthält $ANZAHL Dateien."
else
    echo "Das Verzeichnis $VERZEICHNIS existiert nicht."
fi
```

---

## Aufgabe 12: Log-Dateien filtern

### Aufgabe
Erstelle ein Script, das eine Log-Datei durchsucht und alle Zeilen ausgibt, die ein bestimmtes Schlüsselwort enthalten.

### Anleitung
1. Nutze `grep`, um die Zeilen zu filtern.
2. Akzeptiere die Log-Datei und das Schlüsselwort als Parameter.
3. Beispiel:
   ```bash
   ./script.sh logdatei.txt Fehler
   ```

### Musterlösung
```bash
#!/bin/bash

LOGDATEI=$1
SCHLUESSELWORT=$2

if [ -f "$LOGDATEI" ]; then
    grep "$SCHLUESSELWORT" "$LOGDATEI"
else
    echo "Die Log-Datei $LOGDATEI existiert nicht."
fi
```

---

## Aufgabe 13: Datum und Uhrzeit prüfen

### Aufgabe
Erstelle ein Script, das das aktuelle Datum und die Uhrzeit ausgibt und überprüft, ob es gerade Vormittag oder Nachmittag ist.

### Anleitung
1. Nutze den Befehl `date`, um das aktuelle Datum und die Uhrzeit zu ermitteln.
2. Extrahiere die Stunde mit `date +"%H"`.
3. Nutze eine Bedingung, um zwischen Vormittag (Stunde < 12) und Nachmittag (Stunde >= 12) zu unterscheiden.

### Musterlösung
```bash
#!/bin/bash

ZEIT=$(date +"%H")

if [ "$ZEIT" -lt 12 ]; then
    echo "Guten Morgen! Es ist $(date +"%H:%M")."
else
    echo "Guten Nachmittag! Es ist $(date +"%H:%M")."
fi
```

---

## Aufgabe 14: Backup erstellen

### Aufgabe
Erstelle ein Script, das eine Sicherungskopie eines angegebenen Verzeichnisses in ein Backup-Verzeichnis erstellt. Der Name der Sicherungsdatei soll das aktuelle Datum enthalten.

### Anleitung
1. Nutze `tar`, um das Verzeichnis zu archivieren.
2. Füge das aktuelle Datum mit `date +"%Y%m%d"` zum Dateinamen hinzu.
3. Beispiel:
   ```bash
   ./script.sh zu_sicherndes_verzeichnis backup_verzeichnis
   ```

### Musterlösung
```bash
#!/bin/bash

QUELLE=$1
ZIEL=$2

if [ -d "$QUELLE" ]; then
    DATUM=$(date +"%Y%m%d")
    TAR_NAME="${ZIEL}/backup_${DATUM}.tar.gz"
    tar -czf "$TAR_NAME" "$QUELLE"
    echo "Backup wurde erstellt: $TAR_NAME"
else
    echo "Das Verzeichnis $QUELLE existiert nicht."
fi
```
