## Musterlösungen für Übungen zum Prozessmanagement in Linux

### Musterlösung für Übung 1: Prozesse auflisten und analysieren

#### 1. Grundlegende Prozessinformationen anzeigen

```bash
ps aux
```

- **USER**: Der Besitzer des Prozesses.
- **PID**: Die Prozess-ID.
- **%CPU**: Der Anteil der CPU-Zeit, den der Prozess verwendet.
- **%MEM**: Der Anteil des physischen Speichers, den der Prozess verwendet.
- **VSZ**: Virtuelle Speichergröße des Prozesses.
- **RSS**: Resident Set Size, der Teil des Speichers, der im physischen Speicher gehalten wird.
- **TTY**: Das Terminal, das den Prozess startete.
- **STAT**: Der Status des Prozesses (R running, S sleeping, Z zombie, usw.).
- **START**: Startzeit des Prozesses.
- **TIME**: Gesamte CPU-Zeit, die der Prozess verbraucht hat.
- **COMMAND**: Der Befehl, der den Prozess gestartet hat.

#### 2. Dynamische Prozessansicht

```bash
top
```

- Nutzen Sie Tastenkombinationen wie `P` (CPU-Nutzung sortieren), `M` (Speichernutzung sortieren), um die Ansicht zu ändern.

#### 3. Baumstruktur der Prozesse

```bash
pstree
```

- Zeigt die Prozesshierarchie als Baumstruktur.

### Musterlösung für Übung 2: Spezifische Prozesse finden und filtern

#### 1. Prozesse nach Namen finden

```bash
pgrep bash
```

- Gibt die PID(s) von allen laufenden `bash`-Prozessen aus.

#### 2. Prozesse detailliert filtern

```bash
ps aux | grep ssh
```

- Filtert Prozesse, die `ssh` im Namen haben.

#### 3. Prozesse nach Benutzer filtern

```bash
ps -u root
```

- Listet alle Prozesse auf, die vom Benutzer `root` ausgeführt werden.

### Musterlösung für Übung 3: Prozesse beenden und Priorität ändern

#### 1. Einen Prozess sicher beenden

```bash
kill -SIGTERM [PID]
```

Falls der Prozess nicht reagiert:

```bash
kill -SIGKILL [PID]
```

#### 2. Prozesspriorität ändern

```bash
nice -n 10 command
```

- Startet `command` mit einer niedrigeren Priorität.

```bash
renice 5 -p [PID]
```

- Ändert die Priorität eines laufenden Prozesses.

#### 3. Prozesse neu starten

```bash
kill -SIGTERM [PID] && command
```

- Beendet den Prozess und startet `command` neu.

### Musterlösung für Übung 4: Erweiterte Prozessüberwachung

#### 1. Spezifische Prozessdetails anzeigen

```bash
ps -o pid,ppid,cputime,cmd -p [PID]
```

- Zeigt spezifische Details eines Prozesses an.

#### 2. Überwachung der Prozessaktivität

```bash
watch ps -p [PID]
```

- Überwacht die Aktivität eines spezifischen Prozesses.

#### 3. Benachrichtigung bei Prozessende

```bash
while ps -p [PID] > /dev/null; do sleep 1; done; echo "Prozess [PID] beendet."
```

- Wartet, bis ein spezifischer Prozess beendet wird, und gibt dann eine Nachricht aus.

### Musterlösung für Übung 5: Systemweite Prozessanalyse

#### 1. Systemweite Ressourcennutzung analysieren

```bash
top
```

- `top` nutzen und nach CPU- und Speichernutzung sortieren.

#### 2. Finden von Zombie-Prozessen

```bash
ps aux | grep 'Z'
```

- Listet Zombie-Prozesse auf.

#### 3. Lang laufende Prozesse identifizieren

```bash
ps -eo pid,etime,cmd | sort -k 2 -r
```

- Listet Prozesse nach ihrer Laufzeit sortiert auf.

Diese Musterlösungen bieten eine Grundlage für die Arbeit mit Prozessen in Linux und sollen Ihnen helfen, die Befehlszeilentools effektiv für Prozessmanagementaufgaben zu nutzen.
