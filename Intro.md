# Einführung in Shell-Scripting

## Aufgabe 1: Ein einfaches Script schreiben und ausführen

### Aufgabe
Schreibe ein Shell-Script, das den Text **"Hallo Welt!"** auf der Konsole ausgibt. Speichere das Script unter dem Namen `hallo.sh`.

### Anleitung
1. Öffne einen Texteditor (z. B. `nano`).
2. Schreibe den folgenden Code hinein:
   ```bash
   #!/bin/bash
   echo "Hallo Welt!"
   ```
3. Speichere die Datei als `hallo.sh`.
4. Mache das Script ausführbar:
   ```bash
   chmod +x hallo.sh
   ```
5. Führe das Script aus:
   ```bash
   ./hallo.sh
   ```

---

## Aufgabe 2: Arbeiten mit Variablen

### Aufgabe
Erweitere das Script, sodass es den Namen des Benutzers aus einer Variable begrüßt. Die Ausgabe soll lauten: **"Hallo, [Name]!"**, wobei `[Name]` durch den Inhalt der Variablen ersetzt wird.

### Anleitung
1. Füge eine Variable `NAME` hinzu, die den Namen des Benutzers speichert.
2. Nutze die Variable in der `echo`-Anweisung.
3. Beispielausgabe: `Hallo, Alex!`

### Musterlösung
```bash
#!/bin/bash
NAME="Alex"
echo "Hallo, $NAME!"
```

---

## Aufgabe 3: Benutzerinput verwenden

### Aufgabe
Passe das Script so an, dass der Benutzer seinen Namen eingeben kann. Die Ausgabe soll weiterhin **"Hallo, [Name]!"** lauten.

### Anleitung
1. Nutze den Befehl `read`, um Benutzereingaben zu erfassen.
2. Speichere die Eingabe in einer Variable.
3. Beispiel:
   ```bash
   echo "Wie heißt du?"
   read NAME
   echo "Hallo, $NAME!"
   ```

### Musterlösung
```bash
#!/bin/bash
echo "Wie heißt du?"
read NAME
echo "Hallo, $NAME!"
```

---

## Aufgabe 4: Bedingungen einführen

### Aufgabe
Erweitere das Script so, dass es überprüft, ob der Benutzer "root" ist. Falls ja, gib die Nachricht **"Hallo, Administrator!"** aus. Andernfalls gib die Nachricht **"Hallo, [Name]!"** aus.

### Anleitung
1. Nutze eine `if`-Anweisung.
2. Prüfe, ob der Benutzername `root` entspricht.
3. Nutze `else`, um die Standardnachricht auszugeben.

### Musterlösung
```bash
#!/bin/bash
echo "Wie heißt du?"
read NAME

if [ "$NAME" = "root" ]; then
    echo "Hallo, Administrator!"
else
    echo "Hallo, $NAME!"
fi
```

---

## Aufgabe 5: Optionen verarbeiten

### Aufgabe
Erstelle ein Script, das abhängig von einer übergebenen Option unterschiedlich reagiert. Unterstütze die Optionen:
- `-h` oder `--help`: Zeige eine Hilfemeldung.
- `-n [Name]`: Begrüße den angegebenen Namen.

### Anleitung
1. Nutze `case` oder `if` zur Verarbeitung von Optionen.
2. Verwende `$1`, `$2`, usw., um auf Kommandozeilenargumente zuzugreifen.
3. Beispiel:
   ```bash
   ./script.sh -h
   ./script.sh -n Alex
   ```

### Musterlösung
```bash
#!/bin/bash

if [ "$1" = "-h" ] || [ "$1" = "--help" ]; then
    echo "Verwendung:"
    echo "  -h, --help   Zeige diese Hilfe an"
    echo "  -n [Name]    Begrüße den angegebenen Namen"
elif [ "$1" = "-n" ]; then
    echo "Hallo, $2!"
else
    echo "Ungültige Option. Nutze -h für Hilfe."
fi
```

---

## Aufgabe 6: Dateien und Verzeichnisse prüfen

### Aufgabe
Erstelle ein Script, das überprüft, ob eine Datei existiert und lesbar ist. Falls die Datei existiert, soll eine Nachricht ausgegeben werden: **"Die Datei [Dateiname] existiert und ist lesbar."** Falls nicht, gib eine Fehlermeldung aus.

### Anleitung
1. Nutze den `test`-Befehl oder `[ ... ]`.
2. Überprüfe, ob die Datei mit `[ -f ]` existiert und mit `[ -r ]` lesbar ist.
3. Beispiel:
   ```bash
   ./script.sh datei.txt
   ```

### Musterlösung
```bash
#!/bin/bash

DATEI=$1

if [ -f "$DATEI" ] && [ -r "$DATEI" ]; then
    echo "Die Datei $DATEI existiert und ist lesbar."
else
    echo "Die Datei $DATEI existiert nicht oder ist nicht lesbar."
fi
```

---

## Aufgabe 7: Schleifen verwenden

### Aufgabe
Schreibe ein Script, das die Zahlen von 1 bis 5 in einer Schleife ausgibt.

### Anleitung
1. Nutze eine `for`-Schleife.
2. Iteriere über eine Zahlenfolge.
3. Beispielausgabe:
   ```
   1
   2
   3
   4
   5
   ```

### Musterlösung
```bash
#!/bin/bash

for i in {1..5}; do
    echo "$i"
done
```
