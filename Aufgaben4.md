## Übungsaufgaben zu Linux: Seiteneffekte, Variablen, Umgebungsvariablen und if Statements

### Seiteneffekt mit Operatoren `&&`, `||` und `;`

1. Schreiben Sie ein Bash-Skript, das überprüft, ob eine Datei existiert. Wenn die Datei existiert, soll eine Erfolgsmeldung ausgegeben werden; andernfalls soll eine Fehlermeldung erscheinen. Verwenden Sie den Operator `&&` für die Erfolgsmeldung und `||` für die Fehlermeldung.

2. Verwenden Sie den `;` Operator, um zwei Befehle in einer Zeile auszuführen: Erstellen Sie zuerst eine Datei mit `touch` und zeigen Sie dann den Inhalt des aktuellen Verzeichnisses mit `ls` an.

3. Erweitern Sie das Skript aus Aufgabe 1, indem Sie mit dem `;` Operator einen zusätzlichen Befehl hinzufügen, der unabhängig vom Ergebnis der Dateiüberprüfung ausgeführt wird. Zum Beispiel das Anzeigen des aktuellen Datums mit `date`.

### Variablen und Umgebungsvariablen

4. Schreiben Sie ein Skript, das eine lokale Variable definiert und ihren Wert ausgibt. Ändern Sie dann den Wert der Variablen und geben Sie ihn erneut aus.

5. Modifizieren Sie das Skript aus Aufgabe 4, indem Sie die Variable als Umgebungsvariable exportieren und in einer Subshell ausgeben. Überprüfen Sie, ob die Variable in der Subshell verfügbar ist.

6. Demonstrieren Sie den Unterschied zwischen der Ausführung eines Skripts mit `bash` und `source` (`. Skriptname`). Definieren Sie eine Umgebungsvariable im Skript und zeigen Sie deren Verfügbarkeit in der aktuellen Shell nach Ausführung mit beiden Methoden.

### if Statements

7. Schreiben Sie ein Skript, das eine Variable auf einen bestimmten Wert überprüft. Wenn der Wert erfüllt ist, soll eine Meldung ausgegeben werden; andernfalls soll eine andere Meldung erscheinen.

8. Erweitern Sie das Skript aus Aufgabe 7, indem Sie mehrere Bedingungen mit `elif` hinzufügen, um unterschiedliche Werte der Variablen zu überprüfen. Geben Sie für jeden Wert eine spezifische Meldung aus.

9. Kombinieren Sie die Konzepte der Operatoren `&&`, `||` und if Statements, um ein Skript zu erstellen, das überprüft, ob eine Datei existiert und ob sie schreibbar ist. Verwenden Sie if Statements für die Überprüfung und geben Sie entsprechende Meldungen aus.

Diese Übungsaufgaben decken grundlegende Konzepte der Bash-Programmierung ab, einschließlich der Verwendung von Seiteneffekten durch Operatoren, der Handhabung von Variablen und Umgebungsvariablen sowie der Steuerung des Programmflusses mit if Statements.
