## Musterlösungen zu Linux-Dateibefehlen

### Lösung Aufgabe 1: Auflisten von Dateien und Verzeichnissen

```sh
# 1. Alle Dateien und Verzeichnisse auflisten
ls

# 2. Alle Dateien inklusive versteckter Dateien auflisten
ls -a

# 3. Detaillierte Auflistung
ls -l
```

### Lösung Aufgabe 2: Erstellen und Löschen von Dateien

```sh
# 1. Erstellen einer leeren Datei
touch testdatei.txt

# 2. Löschen der Datei ohne Bestätigungsaufforderung
rm -f testdatei.txt
```

### Lösung Aufgabe 3: Erstellen und Navigieren von Verzeichnissen

```sh
# 1. Neues Verzeichnis erstellen
mkdir neues_verzeichnis

# 2. In das neue Verzeichnis wechseln
cd neues_verzeichnis

# 3. Zum übergeordneten Verzeichnis zurückkehren
cd ..
```

### Lösung Aufgabe 4: Kopieren und Verschieben von Dateien

```sh
# Kopieren der Datei
cp testdatei.txt kopie_testdatei.txt

# Verschieben der kopierten Datei
mv kopie_testdatei.txt neues_verzeichnis/
```

### Lösung Aufgabe 5: Bearbeiten von Dateiberechtigungen

```sh
# Berechtigungen für die Datei ändern
chmod 600 testdatei.txt

# Berechtigungen für das Verzeichnis ändern
chmod 700 neues_verzeichnis
```

### Lösung Aufgabe 6: Durchsuchen von Dateiinhalten

```sh
# Nach Wort "Linux" suchen
grep "Linux" testdatei.txt

# Ersten 10 Zeilen anzeigen
head -n 10 testdatei.txt

# Letzten 10 Zeilen anzeigen
tail -n 10 testdatei.txt
```

### Lösung Aufgabe 7: Verwenden von Pipes und Redirection

```sh
# Ausgabe in Datei umleiten
ls -l > verzeichnisinhalt.txt

# Zeilen, die "testdatei.txt" enthalten, herausfiltern und umleiten
grep "testdatei.txt" verzeichnisinhalt.txt > gefilterte_inhalte.txt
```

### Lösung Aufgabe 8: Anzeigen von Dateitypen

```sh
# Dateityp anzeigen
file testdatei.txt
```

### Lösung Aufgabe 9: Nutzung von Wildcards

```sh
# Mehrere Dateien erstellen
touch datei.txt datei.pdf datei.doc

# Alle .txt Dateien löschen
rm datei*.txt
```

### Lösung Aufgabe 10: Finden von Dateien und Verzeichnissen

```sh
# Dateien größer als 10MB suchen
find . -type f -size +10M

# Verzeichnisse mit bestimmtem Namen suchen
find / -type d -name neues_verzeichnis
```