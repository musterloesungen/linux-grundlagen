# Musterlösung für Linux-Übungsaufgaben

## Lösung zu Aufgabe 1: Dateiverwaltung

```bash
mkdir ~/linux_schulung
cd ~/linux_schulung
touch test.txt
echo "Hallo Linux Welt" > test.txt
ls -l test.txt
ln -s test.txt link_test.txt
mkdir backup
mv test.txt backup/
rm link_test.txt
```

## Lösung zu Aufgabe 2: Erweiterte Dateiverwaltung

```bash
touch file1.txt file2.txt file3.txt
ls -a
ln file1.txt hardlink_file1.txt
ls -i file1.txt hardlink_file1.txt
rm file2.txt
ls
mkdir dir1 dir2
mv file3.txt dir1/
mv dir1/file3.txt dir2/
```

## Lösung zu Aufgabe 3: Nutzung von `man` und `ln`

```bash
man touch # Informationen zum Aktualisieren des Änderungsdatums suchen
man ls > man_ls.txt
ln -s dir2 sym_dir2
ls -l sym_dir2
```

## Lösung zu Aufgabe 4: Datei- und Verzeichnisrechte

```bash
chmod 600 file1.txt
touch secret.txt
chmod 600 secret.txt
ls -l secret.txt
rm -r backup
```

Diese Lösungen bieten einen direkten Weg, um die in den Aufgaben gestellten Anforderungen zu erfüllen. Die Befehle und Konzepte, die hier verwendet werden, sind essentiell für die Arbeit in der Linux-Befehlszeile und bilden eine solide Grundlage für die Teilnehmer der Schulung.
