## Übungsaufgaben für Linux

### Seiteneffekte mit &&, || und ;

1. **Aufgabe: Verwendung von &&**  
   Schreiben Sie ein Skript, das eine Datei mit dem Namen "testfile.txt" im aktuellen Verzeichnis erstellt, wenn sie nicht existiert. Verwenden Sie dafür den Befehl `touch` und die Bedingung mit `&&`.

2. **Aufgabe: Verwendung von ||**  
   Schreiben Sie ein Kommando, das eine Nachricht ausgibt, wenn das Erstellen einer Datei fehlschlägt. Nutzen Sie dazu `touch` für eine Datei, auf die Sie keine Schreibrechte haben, gefolgt von `||` und `echo`.

3. **Aufgabe: Kombination von && und ||**  
   Kombinieren Sie `&&` und `||` in einem Befehl, um eine Datei zu erstellen (mit `touch`). Falls dies erfolgreich ist, soll eine Erfolgsmeldung ausgegeben werden, andernfalls eine Fehlermeldung.

4. **Aufgabe: Verwendung von ;**  
   Schreiben Sie ein Kommando, das zwei Befehle nacheinander ausführt, unabhängig vom Erfolg des ersten Befehls. Nutzen Sie dazu das Semikolon `;` zwischen den Befehlen.

### Variablen und Umgebungsvariablen

1. **Aufgabe: Erstellen und Ausgeben einer Variablen**  
   Erstellen Sie eine lokale Variable `meineVar` mit dem Wert "Hallo Welt" und geben Sie den Wert der Variablen auf der Konsole aus.

2. **Aufgabe: Umgebungsvariable exportieren**  
   Wandeln Sie `meineVar` in eine Umgebungsvariable um und zeigen Sie, wie ein Skript, das in einer Subshell ausgeführt wird, auf diese Variable zugreifen kann.

3. **Aufgabe: Unterschied zwischen Subshell und aktueller Shell**  
   Schreiben Sie ein Skript, das eine Variable in der aktuellen Shell setzt und eine weitere Variable innerhalb einer Subshell (durch ein Skript oder einen Befehl, der in `$(...)` eingeschlossen ist). Demonstrieren Sie, wie sich die Sichtbarkeit der Variablen zwischen der Subshell und der aktuellen Shell unterscheidet.

4. **Aufgabe: Verwendung von `source` zum Setzen von Umgebungsvariablen**  
   Erstellen Sie eine Datei `setvars.sh`, die einige Umgebungsvariablen setzt. Verwenden Sie `source`, um diese Variablen in Ihrer aktuellen Shell verfügbar zu machen. Zeigen Sie, wie sich die Variablen von denen unterscheiden, die in einer Subshell gesetzt wurden.

### if Statements

1. **Aufgabe: Einfaches if Statement**  
   Schreiben Sie ein Skript, das überprüft, ob eine Variable `num` größer als 10 ist. Wenn ja, soll eine entsprechende Nachricht ausgegeben werden.

2. **Aufgabe: if-else Statement**  
   Erweitern Sie das vorherige Skript um eine else-Anweisung, die eine Nachricht ausgibt, falls `num` kleiner oder gleich 10 ist.

3. **Aufgabe: if-elif-else Statement**  
   Modifizieren Sie das Skript, sodass es zusätzlich prüft, ob `num` kleiner als 0 ist. Es soll entsprechende Nachrichten für Zahlen kleiner als 0, größer als 10 und dazwischen ausgeben.

## Musterlösungen

### Seiteneffekte

1. **Lösung:**
   ```bash
   [[ -f "testfile.txt" ]] || touch "testfile.txt" && echo "Datei erstellt oder bereits vorhanden."
   ```

2. **Lösung:**
   ```bash
   touch "/root/testfile.txt" || echo "Fehler beim Erstellen der Datei."
   ```

3. **Lösung:**
   ```bash
   touch "newfile.txt" && echo "Erfolg." || echo "Fehler."
   ```

4. **Lösung:**
   ```bash
   echo "Dies wird immer ausgeführt."; false; echo "Dies wird auch immer ausgeführt."
   ```

### Variablen und Umgebungsvariablen

1. **Lösung:**
   ```bash
   meineVar="Hallo Welt"
   echo $meineVar
   ```

2. **Lösung:**
   ```bash
   export meineVar="Hallo Welt"
   # In einem anderen Skript oder Sub
